/**
 * helm
 */

/* Node modules */

/* Third-party modules */
import Vue from 'vue'; // eslint-disable-line import/no-extraneous-dependencies

/* Files */

export default {
  actions: {
    load({ commit }, entries) {
      commit('setEntries', entries);
    },
  },

  getters: {
    entries: ({ entries }) => entries,
  },

  mutations: {
    setEntries(state, entries) {
      Vue.set(state, 'entries', entries);
    },
  },

  state: () => ({
    entries: [],
  }),
};
