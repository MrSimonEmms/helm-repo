/**
 * helm.server
 */

/* Node modules */

/* Third-party modules */

/* Files */
import helm from '../lib/helm';

export default async ({ store }) => {
  await store.dispatch('helm/load', await helm.load());
};
