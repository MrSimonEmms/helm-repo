/**
 * helm
 */

/* Node modules */
import path from 'path';
import { promises as fs } from 'fs';

/* Third-party modules */
import yaml from 'js-yaml';

/* Files */

export default {
  async load() {
    const { entries } = yaml.safeLoad(
      await fs.readFile(path.join(process.env.root, 'packages', 'index.yaml'), 'utf8'),
    );

    return entries;
  },
};
